Script runs `rostopic echo` on a bag or directory of bags of your choosing, to
extract a given topic.

``` bash
Usage: Bagtractor [OPTIONS]... [-t topic] AND [-b bagfile] OR [-d directory of bags]
A (ros)bag(ex)tractor.
Extracts a given topic from a bag or directory of bags.

  -t, --topic-name   name of topic to extract
  -b,  --bagfile     specify bag filename
  -d,  --bagdir      specify directory of bags

  -O, --output-dir   optional directory in which to save csv(s), default is csv
  -p, --post-fix     optional post-fix to add to filename

  -f, --force        overwrite existing csv files

  -h, --help         display this usage text
  -v, --version      output version and exit
```
